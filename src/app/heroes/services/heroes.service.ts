import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, catchError, map, of } from "rxjs";
import { Hero } from "../interfaces/hero.interface";
import { environments } from "../../../environments/environment";


@Injectable({providedIn:'root'})

export class HeroesService
{
    private baseUrl:string = environments.baseUrl;
    constructor(private http: HttpClient) { }
    
    public getHeroes ():Observable<Hero[]>{
        const url=`${this.baseUrl}/heroes`
        return this.http.get<Hero[]>(url);
    }

    public getHeroById(id:string):Observable<Hero | undefined>{
        const url=`${this.baseUrl}/heroes/${id}`
        return this.http.get<Hero>(url)
                   .pipe(
                    catchError((error)=>
                        of (undefined)
                    )
                   )
    }

    public getSuggestions(query:string):Observable<Hero[]>{
        const url=`${this.baseUrl}/heroes/?q=${query}&_limit=6`;
        return this.http.get<Hero[]>(url);

    }

    public addHero(hero:Hero):Observable<Hero>{
        const url=`${this.baseUrl}/heroes`;
        return this.http.post<Hero>(url,hero);
    }

    public updateHero(hero:Hero):Observable<Hero>{
        const url=`${this.baseUrl}/heroes/${hero.id}`;
        if(!hero.id) throw Error ('Hero id is required');
        return this.http.patch<Hero>(url,hero);
    }

    public deleteHero(id:string):Observable<boolean>{
        const url=`${this.baseUrl}/heroes/${id}`;
        return this.http.delete(url)
            .pipe(
                map(resp=> true),
                catchError(err=> of (false)),
            )
    }
}