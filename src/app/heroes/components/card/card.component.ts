import { Component, Input, OnInit } from '@angular/core';
import { Hero } from '../../interfaces/hero.interface';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styles: ``
})
export class CardComponent implements OnInit{
  
  @Input()
  public heroe!:Hero;

  ngOnInit(): void {
    if (!this.heroe) throw Error ('Hero property is required')
    
  }

}
