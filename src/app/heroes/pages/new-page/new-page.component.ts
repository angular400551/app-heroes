import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Hero, Publisher } from '../../interfaces/hero.interface';
import { HeroesService } from '../../services/heroes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, switchMap } from 'rxjs';
import { MatSnackBar} from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-new-page',
  templateUrl: './new-page.component.html',
  styles: ``
})
export class NewPageComponent implements OnInit {

  public heroForm = new FormGroup({
    id: new FormControl(''),               
    superhero: new FormControl('',{nonNullable:true}),       
    publisher: new FormControl<Publisher>(Publisher.DCComics),
    alter_ego: new FormControl(''),       
    first_appearance: new FormControl(''),
    characters: new FormControl(''),       
    alt_img: new FormControl(''),         
  })

  constructor(private heroesService:HeroesService,
              private activatedRoute:ActivatedRoute,
              private router:Router,
              private matSnackBar:MatSnackBar,
              private dialog: MatDialog){}
  ngOnInit(): void {
    if(!this.router.url.includes('edit')) return;
    this.activatedRoute.params
        .pipe(
          switchMap(({id})=> this.heroesService.getHeroById(id))  
        ).subscribe( hero=>{
          if(!hero) return this.router.navigateByUrl('/');
          this.heroForm.reset(hero);
          return
        })
  }

  public publishers=[
    {
      'description':'DC - Comics',
      'id':'DC Comics'
    },
    {
      'description':'Marvel - Comics',
      'id':'Marvel Comics'
    }
  
  ]

  get currentHero():Hero{
    const hero= this.heroForm.value as Hero;
    return hero
  }

  public onSubmit():void{
    console.log({
      'formIsValid': this.heroForm.valid,
      'value': this.heroForm.value
    });

    if (this.heroForm.invalid) return;
    if(this.currentHero.id){
      // Todo: Actualizar Heroe
      this.heroesService.updateHero(this.currentHero)
          .subscribe(hero=>{
            this.showSnackBar(`${hero.superhero} updated!`)
          })
      return
    }

    this.heroesService.addHero(this.currentHero)
        .subscribe(hero=>{
          this.router.navigate(['/heroes/edit',hero.id])
          this.showSnackBar(`${hero.superhero} created!`);
        })
  }
  
  showSnackBar(message:string):void{
    this.matSnackBar.open(message,'done',{
      duration:2500,
    })
  }

  onDeleteHero(){
    if(!this.currentHero.id) throw Error('Hero id is required');
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: this.currentHero,
    });

    dialogRef.afterClosed()
      .pipe(
        filter(result=> result),
        switchMap(()=> this.heroesService.deleteHero(this.currentHero.id)),
        filter(wasDeleted=>wasDeleted)
      )
      .subscribe(result => {
      this.router.navigate(['/heroes'])
      console.log('deleted');
    });
  }
  
}
